

require('dotenv').load();
require('babel-register');

var path = require('path');
global.ROOT_DIR = path.dirname(require.main.filename);

require('./server')(function(){
    require('./webpack/server')();
});