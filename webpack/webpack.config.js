
const webpack = require('webpack');
const path = require('path');
const host = process.env.HOST || "0.0.0.0";
const port = process.env.WP_DEV_PORT || 3001;
const publicPath = path.resolve(ROOT_DIR, 'public');

module.exports = {
    entry: [
        `webpack-dev-server/client?http://${host}:${port}`,
        "webpack/hot/dev-server",
        path.resolve(ROOT_DIR, 'client/main.js')
    ],
    devtool: "source-map",
    output: {
        path: publicPath,
        filename: 'bundle.js',
        publicPath: `http://${host}:${port}/`
    },

    plugins: [
        new webpack.HotModuleReplacementPlugin(),
    ],

    module:{
        loaders:[
            {test:  /\.(woff|woff2|eot|ttf|svg)$/, loader: 'url'},
            {
                loader: `style-loader!css?modules&localIdentName=[name]_[local]__[hash:base64:5]!resolve-url!sass`,
                test: /\.scss$/,
            },
            {
                test: /\.css$/,
                loader: `style-loader!css`
            },

            {
                test: /\.jsx?$/,
                loaders: ['babel'],
                include: path.resolve(ROOT_DIR, 'client'),
            }
        ]
    }

};