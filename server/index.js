


import express from 'express';
import bodyParser from 'body-parser';
import path from 'path';
import low from 'lowdb';
import {applyChange} from 'deep-diff';



export default callback => {



    let db = low(path.join(ROOT_DIR, 'data/db.json'));

    const app = express();

    app.use(bodyParser.json());
    app.use( express.static( path.join(ROOT_DIR, 'public') ));
    app.set('views', path.join(ROOT_DIR, 'server/views'));
    app.set('view engine', 'pug');

    app.get('/', (req, res) => {
        res.locals.data = db.getState();
        let script = require(path.resolve(ROOT_DIR, 'webpack/webpack.config.js')).output;
        res.locals.script = `${script.publicPath}${script.filename}`;
        res.render('index');
    });

    app.post('/sync', (req, res) => {
        let state = db.getState();
        let diffs = req.body;

        try {
            diffs.forEach(diff => applyChange(state, true, diff));
            db.setState(state);
            res.send('ok');
            console.log("applied changes", diffs);
        } catch(e){
            console.log(e);
            res.send(e);
        }

    });


    app.listen(3000, function(){
        console.log('tagger is listening on localhost:', 3000);
        if(callback){
            callback();
        }
    });

}
