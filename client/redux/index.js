


import ReduxThunk from 'redux-thunk';
import {compose, createStore, applyMiddleware, combineReducers} from 'redux';
import createContentReducer from './reducers/content';
import uiReducer from './reducers/ui';
import undoable from 'redux-undo';
import sync from './actions/sync';
import { autoRehydrate, persistStore } from 'redux-persist';


let enhancer = compose(
    applyMiddleware(ReduxThunk),
    window.devToolsExtension ? window.devToolsExtension() : f => f
);

export default content => {

    const reducers  = {
        content: undoable(createContentReducer(content)),
        ui: uiReducer,
    };

    const reducer = combineReducers(reducers);
    const store = createStore(reducer, enhancer, autoRehydrate());

    persistStore(store, {blacklist: ['content']});

    // store.subscribe(() => store.dispatch(sync()));

    return store;
}

