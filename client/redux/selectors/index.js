


import _ from 'lodash';



function isUnitVisible(filters, unit){
    // console.log(_.intersection(unit.tags, filters.tag));
    // console.log(_.intersection(unit.categories, filters.category));
    // console.log(_.intersection(unit.categories, filters.category));
    // console.log(_.intersection(unit.tags, filters.tag).length > 0);
    return (
        _.intersection(unit.tags, filters.tag).length > 0
        // || _.intersection(unit.categories, filters.category).length > 0
        || filters.sentiment.includes(unit.sentiment)
    )
}

function getVisibleGroups(filters, groups){
    return groups.filter(group => {
        return group.units.filter(unit => {
            return isUnitVisible(filters, unit)
        }).length > 0;
    });
}


export const getVisibleGroupIDs = (filters, groups ) => {
    // console.log(filters);
    // console.log("visible groups", getVisibleGroups(filters,groups));
    return getVisibleGroups(filters, groups).map(group => group.id);
};

export const getVisibleUnitIDs = (filters, groups) => {
    let result = getVisibleGroups(filters, groups).map(group => {
        return group.units.filter(unit => {
            return isUnitVisible(filters, unit)
        }).map(unit => unit.id);
    });

    return _.flatten(result);

};