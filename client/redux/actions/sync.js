



import {diff} from 'deep-diff';
import {last} from 'lodash';
import request from 'superagent';
import {showError} from './ui';

export default () => {
    return (dispatch, getState) => {

        let state = getState();
        let d = diff( last(state.content.past), state.content.present );
        request
            .post('/sync')
            .send(d)
            .end((err, res) =>{
                if(err){
                    dispatch(showError(err));
                } else {
                    console.log(res);
                }
            });
    }
}
