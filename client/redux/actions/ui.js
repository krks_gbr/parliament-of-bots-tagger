
import _ from 'lodash';

const actionTypes = {

    TOGGLE_FILTER: "ui/TOGGLE_CATEGORY_FILTER",
    SELECT_UNIT: "ui/SELECT_UNIT",
    DESELECT_UNIT: "ui/DESELECT_UNIT",
    SET_SELECT_GROUP: "ui/SET_SELECT_GROUP",
    UNSET_SELECT_GROUP: "ui/UNSET_SELECT_GROUP",
    TOGGLE_FILTERS_VISIBILITY: 'ui/TOGGLE_FILTERS_VISIBILITY',
    SHOW_ERROR: 'ui/SHOW_ERROR',
    HIDE_ERROR: 'ui/HIDE_ERROR',
};


export {actionTypes};

export const showError = (message) => {
    return {
        type: actionTypes.SHOW_ERROR,
        payload: message
    }
};

export const hideError = () => {
    return {
        type: actionTypes.HIDE_ERROR,
    }
};

export const toggleFiltersVisibility = () => {

    return {
        type: actionTypes.TOGGLE_FILTERS_VISIBILITY,
    }

};

export const toggleFilter = (value, filterType) => {
    return {
        type: actionTypes.TOGGLE_FILTER,
        payload: {
            value: value,
            filterType: filterType
        }
    }
};


export const toggleUnitSelection = unit => {

    return (dispatch, getState) => {


        let currentSelection = getState().ui.selectedUnits;
        if(currentSelection.length === 0){
            dispatch({
                type: actionTypes.SET_SELECT_GROUP,
                payload: unit.groupID
            });
        }


        let isBeingSelected = !currentSelection.map(unit => unit.id).includes(unit.id);
        if(!isBeingSelected){

            if(currentSelection.length === 1){
                dispatch({
                    type: actionTypes.UNSET_SELECT_GROUP,
                    payload: unit
                });
            }

            dispatch({
                type: actionTypes.DESELECT_UNIT,
                payload: unit
            });

            return;
        }


        dispatch({
            type: actionTypes.SELECT_UNIT,
            payload: unit
        });

    }

};