

const actionTypes = {

    UPDATE_TAGS: 'content/UPDATE_TAGS',
    DELETE_TAG: 'content/DELETE_TAG',
    UPDATE_SENTIMENT: 'content/UPDATE_SENTIMENT',
    UPDATE_TEXT: 'content/UPDATE_TEXT',
    MERGE_UNITS: 'content/MERGE_UNITS',
    ADD_UNIT: 'content/ADD_UNIT',
    REMOVE_UNIT: 'content/REMOVE_UNIT',
    CREATE_TAG: 'content/CREATE_TAG',

};

export {actionTypes};

export const updateTags = (groupID, unitID, newTags) => {

};

export const updateSentiment = (groupID, unitID, newSentiment) => {

};

export const updateText = (groupID, unitID, newText) => {

};

export const mergeUnits = (groupID, unitIDs) => {


};

// text, tags, categories, sentiment
export const addUnit = (groupID, unit) => {

};


export const removeUnit = (groupID, unit) => {

};

export const createTag = tag => {
        return {
            type: actionTypes.CREATE_TAG,
            payload: tag.toLowerCase(),
        }
};

export const deleteTag = tag => {
    return {
        type: actionTypes.DELETE_TAG,
        payload: tag,
    }
};