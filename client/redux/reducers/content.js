



import {actionTypes} from '../actions/content';
import _ from 'lodash';


export default (initialState) => {

    return (state = initialState, action) => {


        switch (action.type){

            case actionTypes.CREATE_TAG: {

                return {
                    ...state,
                    tags: state.tags.concat(action.payload)
                }
            }

            case actionTypes.DELETE_TAG: {
                let newState = _.cloneDeep(state);
                newState.tags = state.tags.filter(tag => tag !== action.payload);
                newState.groups.forEach(group => {
                        group.units.forEach(unit => {
                           unit.tags = unit.tags.filter(tag => tag !== action.payload);
                        });
                });
                return newState
            }

            default:
                return state;
        }

    }

}