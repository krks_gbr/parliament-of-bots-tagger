
import {actionTypes} from '../actions/ui';



let filterTypes  = {

    SENTIMENT: 'sentiment',
    CATEGORY: 'category',
    TAG: 'tag',

};

let initialState = {
    filters: {
        category: [],
        sentiment: [],
        tag: []
    },
    selectedUnits: [],
    filtersVisible: false,
    error: null,
};

export {filterTypes};

export default (state = initialState, action) => {


    switch(action.type){
        case actionTypes.TOGGLE_FILTERS_VISIBILITY: {
            return {
                ...state,
                filtersVisible: !state.filtersVisible
            }
        }


        case actionTypes.TOGGLE_FILTER:{
            let {filterType, value} = action.payload;
            let filters = state.filters[filterType];
            let newFilters = filters.includes(value) ? filters.filter(val => val !== value) : filters.concat(value);
            return {
                ...state,
                filters:{
                    ...state.filters,
                    [filterType]: newFilters
                }
            }
        }

        case actionTypes.SHOW_ERROR: {
            return {
                ...state,
                error: {
                    message: action.payload
                }
            }
        }

        case actionTypes.HIDE_ERROR:{
            return {
                ...state,
                error: null,
            }
        }

        case actionTypes.SELECT_UNIT:{
            return {
                ...state,
                selectedUnits: state.selectedUnits.concat(action.payload)
            }
        }

        case actionTypes.DESELECT_UNIT:{
            return {
                ...state,
                selectedUnits: state.selectedUnits.filter(unit => unit.id != action.payload.id)
            }
        }


        default:
            return state;
    }


}