
import style from './scss/global.scss';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './app/';
import {Provider} from 'react-redux';
import configureStore from './redux';


let store = configureStore(window.__data__);

ReactDOM.render(

    <Provider store={store}>
        <App/>
    </Provider>
    , document.getElementById('app')

);


