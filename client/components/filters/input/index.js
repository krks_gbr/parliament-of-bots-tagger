



import React from 'react';


export default class Input extends React.Component{

    componentWillMount(){
        this.state = {value: ""}
    }

    handleChange(e){
        this.setState({value: e.target.value});
    }

    handleSubmit(e){
        e.preventDefault();
        this.props.onSubmit(this.state.value);
        this.setState({value: ""})
    }
    
    render () {
        let { inputProps } = this.props;
        return (
            <form onSubmit={this.handleSubmit.bind(this)}>
                <input autoComplete="off" {...inputProps} value={this.state.value} onChange={this.handleChange.bind(this)}/>
                <input type="submit" style={{display: 'none'}}/>
            </form>
        )
    }
}




