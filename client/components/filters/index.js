



import React from 'react';
import style from './style.scss';
import Input from './input';



export default props => {

    let {tags,
        sentiments,
        categories,
        createTag,
        deleteTag,
        toggleFilter,
        filterTypes,
        visible,
        activeFilters} = props;

    const Filter = props => {
        let {value, type, active, children} = props;
        return (
            <div className={active ? style.activeFilter : style.filter}
                 onClick={() => toggleFilter(value, type)}>
                {children}
            </div>
        )
    };


    function createFilterList(list, type){
        return list.map((item, i) => {
            return (
                    <Filter active={activeFilters[type].includes(item)}
                            key={i}
                            value={item}
                            type={type}>
                        {item}
                    </Filter>
                )
            }
        )
    }


    let sentimentList = createFilterList(sentiments, filterTypes.SENTIMENT);
    let categoryList = createFilterList(categories, filterTypes.CATEGORY);

    let tagList = tags.sort().map((tag, i) => {
            return (
                <div key={i} className={style.tag}>
                    <span className={style.tagRemover} onClick={() => deleteTag(tag)}>[x]</span>
                    <Filter active={activeFilters[filterTypes.TAG].includes(tag)}
                            value={tag}
                            type={filterTypes.TAG}
                    >
                        {tag}
                    </Filter>
                </div>
            )
        }
    );

    return (
        <div className={visible ? style.container : style.hidden}>
            <div className={style.column}>
                <h1>Tags</h1>
                <div>
                    <Input
                        inputProps={{
                            className: style.input,
                            name: "newtag",
                            type: "text",
                            placeholder: "new tag"
                        }}
                        onSubmit={createTag}
                    />
                    {tagList}
                </div>
            </div>
            <div className={style.column}>
                <h1>Categories</h1>
                <div>{categoryList}</div>
            </div>
            <div className={style.column}>
                <h1>Sentiments</h1>
                <div>{sentimentList}</div>
            </div>

        </div>
    )

};