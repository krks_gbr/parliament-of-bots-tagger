


import React from 'react';
import Unit from '../unit';
import shortid from 'shortid';
import style from './style.scss';
import {connect} from 'react-redux';
import {getVisibleUnitIDs} from '../../redux/selectors';
import {toggleUnitSelection} from '../../redux/actions/ui';




const TaggerGroup = props => {


    let {   tags,
            categories,
            sentiments,
            units,
            visibleUnitIDs,
            selectedUnitIDs,
            dispatch
    } = props;


    let unitContainers =
        units   .filter(unit => visibleUnitIDs.includes(unit.id))
                .map(
                        unit => <Unit
                                   key={unit.id}
                                   handleUpdate={f => f}
                                   tags={tags}
                                   categories={categories}
                                   sentiments={sentiments}
                                   unit={unit}
                                   isSelected={selectedUnitIDs.includes(unit.id)}
                                   toggleSelection={() => dispatch(toggleUnitSelection(unit))}
                        />
        );

    return (
        <div className={style.groupTagger}>
            <div className={style.group}>
                <div className={style.units}>
                    {unitContainers}
                </div>
            </div>
        </div>
    )
};


export default connect(state => ({
    visibleUnitIDs: getVisibleUnitIDs(state.ui.filters, state.content.present.groups),
    selectedUnitIDs: state.ui.selectedUnits.map(unit => unit.id)
}))(TaggerGroup);



function mergeSelectedUnits(){
    let selection  = [];

    let mergedID = shortid.generate();
    let mergedSentiment = selection[0].sentiment;
    let mergedTags = [];
    let mergedCategories = [];
    let mergedText = "";
    selection.forEach(unit => {
        mergedTags = mergedTags.concat(unit.tags);
        mergedCategories = mergedCategories.concat(unit.categories);
        mergedText += unit.text +" ";
    });

    let mergedUnit = {
        id: mergedID,
        sentiment: mergedSentiment,
        tags: mergedTags,
        categories: mergedCategories,
        text: mergedText
    };

    console.log('merged unit:', mergedUnit);

    let selectedIDs = selection.map(unit => unit.id);

    let units =  this.state.units;
    let index = units.findIndex(unit => unit.id == selection[0].id);
    units = units.filter(unit => !selectedIDs.includes(unit.id) || unit.tags.length > 0);
    units.splice(index, 0, mergedUnit);

    console.log('new units:', JSON.stringify(units, null, 4));


}