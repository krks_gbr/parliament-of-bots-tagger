

import React from 'react';
import style from './style.scss';
import {connect} from 'react-redux';


const Header = ({toggleFiltersVisibility}) => {
    return (
        <div className={style.header}>
            <div className={style.item} onClick={toggleFiltersVisibility}>
                Filters
            </div>
        </div>
    )
};

export default connect()(Header);