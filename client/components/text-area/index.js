

import React from 'react';

class TextArea extends React.Component{



    componentWillMount(){
        this.state = { value: this.props.value }
    }

    handleChange(e){
        this.setState({value: e.currentTarget.value})
    }

    handleBlur(e){
        this.props.onBlur(this.state.value);
    }

    render(){
        return (
            <textarea {...this.props}
                onChange={this.handleChange.bind(this)}
                onBlur={this.handleBlur.bind(this)}
                value={this.state.value}
            />
        )
    }

}

export default TextArea;