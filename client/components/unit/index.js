



import React from 'react';
import TextArea from '../text-area';
import style from './style.scss';
import Select from 'react-select';
import 'react-select/dist/react-select.css';



class Unit extends React.Component{
    

    render () {

        const { unit,
                tags,
                categories,
                sentiments,
                toggleSelection,
                isSelected,
            } = this.props;

        function mapToSelectOptions(list){
            return list.map(item => ({label: item, value: item}));
        }

        return (
            <div className={isSelected ? style.selected : style.unit}>
                {/*<div className={isSelected ? style.unitSelectorActive : style.unitSelector} onClick={toggleSelection}/>*/}
                <TextArea
                    className={style.sentence}
                    value={unit.text}
                    onBlur={text => console.log(text)}
                />
                <div className={style.selects}>
                    <Select
                        name="tags"
                        multi={true}
                        options={mapToSelectOptions(tags)}
                        value={unit.tags}
                        placeholder="choose a tag"
                        onChange={val => console.log(val)}
                    />

                    <Select
                        name="categories"
                        multi={true}
                        options={mapToSelectOptions(categories)}
                        value={unit.categories}
                        placeholder="choose a category"
                        onChange={val => console.log(val)}
                    />

                    <Select
                        name="sentiment"
                        options={mapToSelectOptions(sentiments)}
                        value={unit.sentiment}
                        placeholder="choose a sentiment"
                        onChange={val => console.log(val)}
                    />
                </div>
            </div>
        )
    }
}

export default Unit;




