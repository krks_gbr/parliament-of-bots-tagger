



import React from 'react';
import Group from '../components/tagger-group';
import Header from '../components/header';
import {connect} from 'react-redux';
import style from './style.scss';
import Filters from '../components/filters';
import {filterTypes} from '../redux/reducers/ui';
import * as uiActions from '../redux/actions/ui';
import * as contentActions from '../redux/actions/content';
import { ActionCreators } from 'redux-undo';
import Error from '../components/error';
import {getVisibleGroupIDs} from '../redux/selectors';


const App = ({ui, content, visibleGroupIDs, dispatch}) => {

    let {categories, tags, sentiments, groups} = content;

    let taggerGroups = groups.filter(group => visibleGroupIDs.includes(group.id) ).map( group =>
        <Group key={group.id}
               groupID={group.id}
               units={group.units}
               tags={tags}
               categories={categories}
               sentiments={sentiments}
        />
    );

    return (
        <div className={style.app}>
            <Header
                tags={tags}
                onTagSubmit={tag => console.log("submit")}
                toggleFiltersVisibility={() => dispatch(uiActions.toggleFiltersVisibility())}
                undo={() => dispatch(ActionCreators.undo())}
                redo={() => dispatch(ActionCreators.redo())}
            />

            <div className={style.taggers}>
                {taggerGroups}
            </div>

            <Filters tags={tags}
                     createTag={tag => dispatch(contentActions.createTag(tag))}
                     deleteTag={tag => dispatch(contentActions.deleteTag(tag))}
                     visible={ui.filtersVisible}
                     activeFilters={ui.filters}
                     sentiments={sentiments}
                     categories={categories}
                     filterTypes={filterTypes}
                     toggleFilter={(value, filterType) => dispatch(uiActions.toggleFilter(value, filterType))}
            />

            {
                ui.error &&
                <Error close={() => dispatch(uiActions.hideError())} message={ui.error.message}/>
            }

        </div>
    )
};


export default connect(state => ({
    ui: state.ui,
    content: state.content.present,
    visibleGroupIDs: getVisibleGroupIDs(state.ui.filters, state.content.present.groups)
}))(App);